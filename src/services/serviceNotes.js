const result = [
  { text: 'Lavar o carro', id: new Date().getTime(), isEditing: false },
  { text: 'Dar vacina na Alice', id: new Date().getTime(), isEditing: false },
];

const listNotes = () => {
  return new Promise((resolve, reject) => {
    resolve(result);
  });
};

const saveNotes = text => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      result.push({
        text: text,
        id: new Date().getTime(),
        isEditing: false,
      });
      resolve();
    }, 100);
  });
};

const deleteNote = index => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      delete result[index]
      resolve();
    }, 500);
  });
};

module.exports = {
  listNotes,
  saveNotes,
  deleteNote,
};
