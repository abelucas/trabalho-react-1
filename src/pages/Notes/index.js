import React, { Component } from 'react';
import Header from '../../components/Header';

import { listNotes, saveNotes, deleteNote } from '../../services/serviceNotes';

import './style.css';

export default class Notes extends Component {
  state = {
    notes: [],
    isSearching: false,
    loading: true,
    searchTerm: '',
    searchList:[],
    loadingSave: false,
    text: '',
    editedText: '',
  };

  async componentDidMount() {
    this.setState({
      notes: await listNotes(),
      loading: false,
    });
  }

  reverseList = () => {
    this.setState({
      notes: this.state.notes.reverse(),
    });
  };

  delNote = index => {
    delete this.state.notes[index];
    deleteNote(index)
    this.setState({
      notes: this.state.notes,
    });
  };

  handleDelete = (i, event) => {
    this.delNote(i);
  };

  handleEdit = (i, event) => {
    this.editNote(i);
  };

  handleSave = (i, event) => {
    this.saveEdit(i);
  };

  search = () => {
    const word = this.state.searchTerm;
    const achou = this.state.notes.filter(it => it.text.includes(word));
    this.setState({
      searchList: achou,
    })
  };

  newNote = () => {
    this.setState({
      new: true,
    });
  };

  editNote = (i, event) => {
    let teste = [...this.state.notes];
    teste[i].isEditing = true;
    this.setState({
      notes: teste,
    });
  };

  saveEdit = (i, event) => {
    let teste = [...this.state.notes];
    teste[i].text = this.state.editedText;
    teste[i].isEditing = false;
    this.setState({
      notes: teste,
      editedText: '',
    })
  };
  searchText = event => {
    this.setState({
      searchTerm: event.target.value,
    });
  };

  editText = event => {
    this.setState({
      text: event.target.value,
    });
  };

  editedText = event => {
    this.setState({
      editedText: event.target.value,
    });
  };

  saveNote = async () => {
    this.setState({
      loadingSave: true,
    });
    await saveNotes(this.state.text);
    this.setState({
      notes: await listNotes(),
      loadingSave: false,
      new: false,
      text: '',
    });
  };

  render() {
    return (
      <div>
        <Header />
        <div className="content">
          <div className="filters">
            <div className="left">
              <input
                onChange={this.searchText}
                type="search"
                placeholder="Procurar..."
              />
              <button onClick={this.search}>Search</button>
            </div>
            <div className="right">
              <button onClick={this.newNote}>Nova nota</button>
            </div>
          </div>

          <hr />

          <div className="list">
            {this.state.loading && (
              <div className="item item-empty">Carregando anotações...</div>
            )}
            {!this.state.notes.length && !this.state.loading && (
              <div className="item item-empty">
                Nenhuma nota criada até o momento!
              </div>
            )}
            {this.state.new && (
              <div className="item">
                <div className="right">
                  {!this.state.loadingSave && (
                    <button onClick={this.saveNote}>Salvar</button>
                  )}
                  {this.state.loadingSave && <span>Salvando...</span>}
                </div>
              </div>
            )}
            <input
              onChange={this.editText}
              className="left"
              type="text"
              placeholder="Digite sua anotação"
              onKeyPress={event => {
                if (event.key === 'Enter') {
                  this.saveNote();
                }
              }}
            />
            <button onClick={this.saveNote}>Salvar</button>
            <button onClick={this.reverseList}>Inverter lista</button>
            <br />
            <br />
            <br />
            {this.state.searchList.length <= 0 && this.state.notes.map((note, index) => (
              <div key={index} className="item">
                {note.id && !note.isEditing && (
                  <div className="note">{note.text}</div>
                )}
                {note.id && note.isEditing && (
                  <input
                    defaultValue={note.text}
                    onChange={this.editedText}
                    className="left"
                    type="text"
                    placeholder="Digite sua anotação"
                    onKeyPress={event => {
                      if (event.key === 'Enter') {
                        this.saveNote();
                      }
                    }}
                  />
                )}
                <div>
                  {note.id && !note.isEditing && (
                    <div className="right">
                      {note.id && (
                        <button onClick={this.handleDelete.bind(this, index)}>
                          Excluir
                        </button>
                      )}
                      <button onClick={this.handleEdit.bind(this, index)}>
                        Editar
                      </button>
                    </div>
                  )}
                  {note.id && note.isEditing && (
                    <div className="right">
                      <button onClick={this.handleSave.bind(this, index)}>
                        Salvar
                      </button>
                    </div>
                  )}
                </div>
              </div>
            ))}
            {this.state.searchList.length > 0 && this.state.searchList.map((note, index) => <div key={index} className="item">
              {note.id && !note.isEditing && (
                <div className="note">{note.text}</div>
              )}
              {note.id && note.isEditing && (
                <input
                  defaultValue={note.text}
                  onChange={this.editedText}
                  className="left"
                  type="text"
                  placeholder="Digite sua anotação"
                  onKeyPress={event => {
                    if (event.key === 'Enter') {
                      this.saveNote();
                    }
                  }}
                />
              )}
              
            </div>)}
          </div>
        </div>
      </div>
    );
  }
}
